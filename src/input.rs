use crate::annot::AnnotFile;
use crate::err::RatlatError;
use crate::lag::Lag;
use crate::space::{ProbMap, Space};
use crate::window::Window;
use hound::WavReader;
use std::ops::Rem;

#[derive(Debug)]
pub struct Input {
    wave_files: Vec<(String, usize)>,
    samples: usize,
    sound_speed: f64,
    smooth: usize,
    annotations: AnnotFile,
    space: Space,
}

impl Input {
    pub fn new<'a, I, J>(
        wave_paths: I,
        mics: J,
        annotation_file: &str,
        sound_speed: f64,
        smooth: usize,
        ignore_freq: bool,
        bounding_box: Option<((f64, f64), (f64, f64))>,
    ) -> Result<Input, RatlatError>
    where
        I: IntoIterator<Item = &'a str>,
        J: IntoIterator<Item = &'a [f64; 3]>,
    {
        if sound_speed <= 0. {
            Err(RatlatError::NonsenseC)?;
        }
        let mut microphones: Vec<[f64; 3]> = mics.into_iter().cloned().collect();
        if microphones.len() < 2 {
            Err(RatlatError::TooLittleMics)?;
        }
        let wave_paths: Vec<String> = wave_paths.into_iter().map(String::from).collect();
        if microphones.len() != wave_paths.len() {
            Err(RatlatError::WavesMicsNotEqual)?;
        }
        let mut sample_rate: Option<u32> = None;
        let mut samples: Option<u32> = None;
        let b_box = match bounding_box {
            None => mics_to_bbox(&microphones),
            Some(((xa, ya), (xb, yb))) => {
                let xr = if xa < xb { (xa, xb) } else { (xb, xa) };
                let yr = if ya < yb { (ya, yb) } else { (yb, ya) };
                ((xr.0, yr.0), (xr.1, yr.1))
            }
        };
        let mut wave_files: Vec<(String, usize)> = Vec::new();
        for e in &wave_paths {
            let (filename, ch) = if let Some((x, chn)) = e.rsplit_once("::") {
                (String::from(x), Some(chn.parse::<usize>()?))
            } else {
                (e.clone(), None as Option<usize>)
            };
            let wr = WavReader::open(&filename)?;

            let spec = wr.spec();
            let ch = match (spec.channels, ch) {
                (1, None) => 1,
                (_, None) => Err(RatlatError::MultichannelUnspecified)?,
                (chmax, Some(chg)) => {
                    if (chg > chmax as usize) | (chg == 0) {
                        Err(RatlatError::MultichannelWrong(chg, chmax as usize))?
                    } else {
                        chg
                    }
                }
            };

            if *sample_rate.get_or_insert(spec.sample_rate) != spec.sample_rate {
                Err(RatlatError::WavesDiscordant)?;
            }
            let dur = wr.duration();
            if *samples.get_or_insert(dur) != dur {
                Err(RatlatError::WavesDiscordant)?;
            }
            wave_files.push((filename, ch - 1));
        }
        eprintln!("Working with sample rate of {}Hz", sample_rate.unwrap());
        let annotations =
            AnnotFile::from_file(annotation_file, sample_rate.unwrap() as f64, ignore_freq)?;
        if annotations.max_sample() > samples.unwrap() as usize {
            eprintln!(
                "Anot max samples is {} but samples is {}",
                annotations.max_sample(),
                samples.unwrap()
            );
            Err(RatlatError::AnnotDiscordant)?;
        }
        let space = Space::new(
            microphones.drain(..),
            sound_speed,
            sample_rate.unwrap() as f64,
            b_box,
        )?;
        Ok(Input {
            wave_files,
            samples: samples.unwrap() as usize,
            sound_speed,
            smooth,
            annotations,
            space,
        })
    }
    pub fn episode_bound_samp(&self, ep: usize) -> (usize, usize) {
        let x = &self.annotations.episodes[ep];
        (x.ts, x.te)
    }
    pub fn episode_bound_freqi(&self, ep: usize) -> Option<(usize, usize)> {
        self.annotations.episodes[ep].fr
    }
    pub fn sample_rate(&self) -> f64 {
        self.space.sampling()
    }
    pub fn episode_bound_freq(&self, ep: usize) -> (f64, f64) {
        match self.episode_bound_freqi(ep) {
            Some((li, ui)) => {
                let (a, b) = self.episode_bounds_time(ep);
                let l = b - a;
                ((li as f64) / l, (ui as f64) / l)
            }
            None => (0., self.sample_rate() / 2.),
        }
    }
    pub fn episode_bounds_time(&self, ep: usize) -> (f64, f64) {
        let x = self.episode_bound_samp(ep);
        let s = self.sample_rate();
        ((x.0 as f64) / s, (x.1 as f64) / s)
    }
    pub fn episodes(&self) -> usize {
        self.annotations.episodes()
    }
    pub fn dim(&self) -> (usize, usize) {
        self.space.dim()
    }
    fn cut_episode(&self, idx: usize, margin_samples: usize) -> Result<Vec<Vec<f32>>, RatlatError> {
        if margin_samples > self.samples {
            Err(RatlatError::MarginLonger)?;
        }
        let ep = &self
            .annotations
            .episodes
            .get(idx)
            .ok_or(RatlatError::InvalidEpisode)?;
        let ts = usize::max(ep.ts, margin_samples) - margin_samples;
        let te = usize::min(ep.te, self.samples - margin_samples) + margin_samples;
        let mut ans: Vec<Vec<f32>> = Vec::with_capacity(self.wave_files.len());
        for (f, chan) in self.wave_files.iter() {
            let mut reader = WavReader::open(f)?;
            let chans = reader.spec().channels;
            reader.seek(ts as u32)?;
            let part: Vec<f32> = if chans == 1 {
                reader
                    .samples::<i32>()
                    .take(te - ts)
                    .map(|x| x.unwrap() as f32)
                    .collect()
            } else {
                reader
                    .samples::<i32>()
                    .take(te - ts)
                    .enumerate()
                    .filter_map(|(i, s)| {
                        if i.rem(chans as usize) == (*chan) {
                            Some(s.unwrap() as f32)
                        } else {
                            None
                        }
                    })
                    .collect()
            };
            ans.push(part);
        }
        Ok(ans)
    }
    pub fn laterate(&self, idx: usize, margin_samples: usize) -> Result<ProbMap<'_>, RatlatError> {
        //TODO: Do not push fr if some configuration option is off...
        let mut lag = Lag::from_vecs(
            &self.cut_episode(idx, margin_samples)?,
            self.smooth,
            Window::Rectangular,
            self.episode_bound_freqi(idx),
        );
        Ok(self.space.laterate(&mut lag))
    }
}

fn mics_to_bbox(p: &Vec<[f64; 3]>) -> ((f64, f64), (f64, f64)) {
    let min_x = p
        .iter()
        .map(|x| x[0])
        .min_by(|a, b| a.partial_cmp(b).unwrap())
        .unwrap();
    let min_y = p
        .iter()
        .map(|x| x[1])
        .min_by(|a, b| a.partial_cmp(b).unwrap())
        .unwrap();
    let max_x = p
        .iter()
        .map(|x| x[0])
        .max_by(|a, b| a.partial_cmp(b).unwrap())
        .unwrap();
    let max_y = p
        .iter()
        .map(|x| x[1])
        .max_by(|a, b| a.partial_cmp(b).unwrap())
        .unwrap();
    ((min_x, min_y), (max_x, max_y))
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn full() {
        let mics: [[f64; 3]; 4] = [
            [-0.3, -0.3, 0.2],
            [-0.3, 0.3, 0.2],
            [0.3, 0.3, 0.2],
            [0.3, -0.3, 0.2],
        ];
        let files = [
            "examples/extract12_ch1.wav",
            "examples/extract12_ch2.wav",
            "examples/extract12_ch3.wav",
            "examples/extract12_ch4.wav",
        ];
        let x = Input::new(
            files.iter().map(|x| *x),
            &mics,
            "examples/extract12_annot.txt",
            344.,
            20,
            true,
            None,
        )
        .unwrap();
        assert_eq!(x.episodes(), 20);
        let off = 300;
        for e in 14..15 {
            let fix = x.laterate(e, off).unwrap();
            println!("Episode {} {:?}", e, fix.get_max());
            let f_n: String = format!("/tmp/lat_{}.png", e);
            fix.render_image(&f_n).unwrap();
        }
    }
}
