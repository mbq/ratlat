use crate::window::Window;
use claxon::FlacReader;
use fftw::array::AlignedVec;
use fftw::plan::{Plan, Plan32, R2CPlan};
use fftw::types::{c32, Flag};

pub struct Spectrogram {
    result: Vec<f32>,
    height: usize,
}

impl Spectrogram {
    pub fn from_flac(
        file_name: &str,
        window_size: usize,
        hop: usize,
        window: Window,
        chan: usize,
        offset: usize,
        len: Option<usize>,
    ) -> Result<Spectrogram, ()> {
        if let Ok(mut reader) = FlacReader::open(file_name) {
            let si = reader.streaminfo();
            let channels = si.channels;
            assert!(channels > chan as u32);
            let n = len.unwrap_or_else(|| si.samples.unwrap() as usize);
            let mut data: Vec<f32> = Vec::with_capacity(n);
            let it = reader
                .samples()
                .skip(offset * (channels as usize))
                .take(n * (channels as usize));
            for (i, v) in it.enumerate() {
                let ch = i % (channels as usize);
                if ch == chan {
                    let val = v.unwrap() as f32;
                    data.push(val);
                }
            }
            Ok(Spectrogram::from_slice(&data, window_size, hop, window))
        } else {
            Err(())
        }
    }
    pub fn from_slice(x: &[f32], window_size: usize, hop: usize, window: Window) -> Spectrogram {
        //Window size: 1024
        let mut buf_inp: AlignedVec<f32> = AlignedVec::new(window_size);
        let mut fwd_plan: Plan<f32, c32, Plan32> =
            R2CPlan::aligned(&[window_size], Flag::ESTIMATE).unwrap();
        let mut buf_out: AlignedVec<c32> = AlignedVec::new(window_size / 2 + 1);
        let mut result: Vec<f32> = Vec::with_capacity(x.len());
        let norm: f32 = x.iter().map(|x| x * x).sum::<f32>().sqrt().max(0.00001);
        for chunk in x.windows(window_size).step_by(hop) {
            for (i, &val) in chunk.iter().enumerate() {
                let v = val as f32;
                buf_inp[i] = v * window.val(i, window_size);
            }
            fwd_plan.r2c(&mut buf_inp, &mut buf_out).unwrap();
            for val in buf_out.iter() {
                result.push(val.norm() / norm);
            }
        }
        Spectrogram {
            result,
            height: window_size / 2 + 1,
        }
    }
    pub fn render_to_file(&self, file_name: &str) -> Result<(), ()> {
        use crate::adam_pal::adam_colour;
        use image::ImageBuffer;
        let mut img =
            ImageBuffer::new((self.result.len() / self.height) as u32, self.height as u32);
        let max = *self
            .result
            .iter()
            .max_by(|&a, &b| a.partial_cmp(b).unwrap())
            .unwrap_or(&0.);

        for (x, y, pix) in img.enumerate_pixels_mut() {
            let yr: usize = (self.height as isize - 1 - y as isize) as usize;
            let val = if max > 0. {
                self.result[(x as usize) * self.height + yr] / max
            } else {
                0.
            };
            let lval = ((val * 0.99 + 0.01).log10() + 2.) / 2.;
            *pix = image::Rgb(adam_colour(lval));
        }
        img.save(file_name).unwrap();

        Ok(())
    }
}
