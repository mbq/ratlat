use std::collections::VecDeque;

pub fn max_filter<T: PartialOrd + Clone + Copy>(x: &[T], before: usize, after: usize) -> Vec<T> {
    //Edge case, empty input, empty vector
    if x.is_empty() {
        return Vec::new();
    }

    //Just max
    let k = after + before + 1;
    if x.len() <= k {
        let m = *x
            .iter()
            .max_by(|&&a, &b| a.partial_cmp(b).unwrap())
            .unwrap();
        return std::iter::repeat(m).take(x.len()).collect();
    }

    let mut dq: VecDeque<usize> = VecDeque::with_capacity(k);
    let mut ans: Vec<T> = Vec::new();

    //Put start of the vector as afters
    for e in 0..=after {
        while let Some(&ee) = dq.front() {
            if x[e] >= x[ee] {
                dq.pop_front();
            } else {
                break;
            }
        }
        dq.push_front(e);
    }

    //Regular
    for e in 0..x.len() {
        ans.push(x[*dq.back().unwrap()]);
        //Remove back elements older than b
        while let Some(&ee) = dq.back() {
            if ee + before <= e {
                dq.pop_back();
            } else {
                break;
            }
        }
        //Remove smaller than cur+a
        if e + after + 1 < x.len() {
            while let Some(&ee) = dq.front() {
                if x[e + after + 1] >= x[ee] {
                    dq.pop_front();
                } else {
                    break;
                }
            }
            dq.push_front(e + after + 1);
        }
    }
    ans
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn mf() {
        let a: Vec<i64> = vec![1, -2, 3, -4, 5, -4, 3, -2, 1];
        let b = max_filter(&a, 1, 1);
        assert_eq!(b, vec![1, 3, 3, 5, 5, 5, 3, 3, 1]);
    }

}
