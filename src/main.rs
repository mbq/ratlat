mod adam_pal;
mod annot;
mod err;
mod input;
mod lag;
mod max_filter;
mod prog;
mod space;
mod spectrogram;
mod window;

use spectrogram::Spectrogram;
use window::Window;

fn spectro_test() {
    let sample_spectro = Spectrogram::from_flac(
        "examples/samp_4_chan_one.flac",
        1024,
        64,
        Window::Rectangular,
        3,
        0,
        None,
    )
    .unwrap();
    sample_spectro
        .render_to_file("/tmp/sample_spectro_rect.png")
        .unwrap();
    let sample_spectro = Spectrogram::from_flac(
        "examples/samp_4_chan_one.flac",
        1024,
        64,
        Window::Hann,
        3,
        0,
        None,
    )
    .unwrap();
    sample_spectro
        .render_to_file("/tmp/sample_spectro_hann.png")
        .unwrap();
}

use crate::prog::{app, Prog};

fn main() {
    let app = app();
    let m = app.get_matches();
    match Prog::from_arg_matches(&m) {
        Ok(prog) => match prog.perform() {
            Err(x) => eprintln!("Error {}", x),
            _ => (),
        },
        Err(x) => eprintln!("Error: {}", x),
    };
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn spectro() {
        spectro_test()
    }
}
