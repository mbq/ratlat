use crate::err::RatlatError;
use crate::input::Input;
use clap::{App, AppSettings, Arg, ArgMatches};
use indicatif::ProgressBar;
use rayon::prelude::*;
use std::fs::{self, File};
use std::io::{BufWriter, Seek, Write};

#[derive(Debug)]
pub struct Prog {
    input: Input,
    heatmap_dir: Option<String>,
    bin_file: Option<String>,
    fix_file: Option<String>,
    padding: usize,
}
type Outcome = Result<Option<(f64, f64, f32)>, RatlatError>;

impl Prog {
    pub fn from_arg_matches(m: &ArgMatches) -> Result<Prog, RatlatError> {
        let (mic_files, mics) = get_mic(m)?;
        let ann = m.value_of("annotation").unwrap();
        let c = get_c(m)?;
        let padding = get_padding(m)?;
        let smoothing = get_smoothing(m)?;
        let ignore_freq = get_ignorefreq(m);
        let bbox = get_bb(m)?;
        let input = Input::new(
            mic_files.iter().map(|x| x.as_str()),
            &mics,
            &ann,
            c,
            smoothing,
            ignore_freq,
            bbox,
        )?;

        Ok(Prog {
            input,
            heatmap_dir: m.value_of("heat maps").map(|x| x.to_string()),
            bin_file: m.value_of("binary map").map(|x| x.to_string()),
            fix_file: m.value_of("fixes").map(|x| x.to_string()),
            padding,
        })
    }
    pub fn perform(&self) -> Result<(), RatlatError> {
        if self.heatmap_dir.is_none() && self.fix_file.is_none() {
            Err(RatlatError::NothingToDo)?;
        }
        //Crate directory for maps
        if let Some(dir) = &self.heatmap_dir {
            fs::create_dir_all(dir)?;
        }
        //Create binary map file
        let bin_file = if let Some(bfn_temp) = &self.bin_file {
            let wh = self.input.dim();
            let eps = self.input.episodes() as u64;
            let dim_string: String = format!("{}x{}x{}", wh.0, wh.1, eps);
            let bfn = bfn_temp.replacen("DIM", &dim_string, 1);
            let jump = (wh.0 as u64) * (wh.1 as u64) * 4;
            //TODO: In bfn, change DIM into wxhxeps
            let bf = File::create(&bfn)?;
            bf.set_len(jump * eps)?;
            Some((bfn, jump))
        } else {
            None
        };
        let bar = ProgressBar::new(self.input.episodes() as u64);
        bar.set_style(
            indicatif::ProgressStyle::default_bar()
                .template("Done episodes {pos}/{len} {wide_bar:.cyan/blue} eta: {eta}"),
        );
        let mut ans: Vec<Outcome> = (0..(self.input.episodes()))
            .into_par_iter()
            .map(|e| {
                let fix = self.input.laterate(e, self.padding)?;
                if let Some(dir) = &self.heatmap_dir {
                    let f_n: String = format!("{}/hm_{:05}.png", dir, e + 1);
                    fix.render_image(&f_n)?;
                }
                if let Some((bfn, jump)) = &bin_file {
                    let mut f = std::fs::OpenOptions::new().write(true).open(&bfn)?;
                    f.seek(std::io::SeekFrom::Start(jump * (e as u64)))?;
                    fix.render_bin(&mut BufWriter::new(f))?;
                }
                if let Some(_) = &self.fix_file {
                    Ok(fix.get_max())
                } else {
                    Ok(None)
                }
            })
            .inspect(|_| bar.inc(1))
            .collect::<Vec<_>>();
        let mut errs: Vec<RatlatError> = Vec::new();
        let mut fixes: Vec<(usize, f64, f64, f32)> = Vec::new();
        for e in ans.drain(..).enumerate() {
            match e {
                (_, Err(x)) => errs.push(x),
                (idx, Ok(Some((x, y, v)))) => fixes.push((idx, x, y, v)),
                _ => (),
            }
        }
        if let Some(f_n) = &self.fix_file {
            let mut f = fs::File::create(f_n)?;
            write!(f, "# Ratlat fixes\n")?;
            write!(f, "Episode\tTs\tTe\tX\tY\tFqL\tFqH\tQuality\n")?;
            for x in &fixes {
                let tb = self.input.episode_bounds_time(x.0);
                let fb = self.input.episode_bound_freq(x.0);
                write!(
                    f,
                    "ep{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n",
                    x.0 + 1,
                    tb.0,
                    tb.1,
                    x.1,
                    x.2,
                    fb.0,
                    fb.1,
                    x.3
                )?;
            }
        }
        bar.finish_and_clear();
        if errs.is_empty() {
            Ok(())
        } else {
            Err(RatlatError::Many(errs))
        }
    }
}

pub fn app<'a, 'b>() -> App<'a, 'b> {
    App::new("ratlat")
        .version("0.0.1")
        .author("Miron B. Kursa <m@mbq.me>")
        .about("Multi-laterate rodent positions from USVs")
        .arg(mic_arg())
        .arg(bb_arg())
        .arg(bin_arg())
        .arg(ann_arg())
        .arg(hm_arg())
        .arg(fix_arg())
        .arg(c_arg())
        .arg(smoothing_arg())
        .arg(padding_arg())
        .arg(ignorefreq_arg())
        .setting(AppSettings::ColorNever)
        .setting(AppSettings::ArgRequiredElseHelp)
}

fn mic_arg<'a, 'b>() -> Arg<'a, 'b> {
    Arg::with_name("microphone")
        .help("Define microphone by its output as a *.wav file and three coordinates, given in meters. Sources are assumed to move in (x,y,0) plane with z axis facing up. To select the channel of a *.wav file, append ::<channel number> to its path; first channel is ::1, this is not required when there is only one channel in file.")
        .long("mic")
        .short("m")
        .value_names(&["wave file", "x", "y", "z"])
        .multiple(true)
        .allow_hyphen_values(true)
        .required(true)
        .number_of_values(4)
}
fn get_mic(m: &ArgMatches) -> Result<(Vec<String>, Vec<[f64; 3]>), RatlatError> {
    let mics = m.occurrences_of("microphone");
    //If mics is too little, attempt to construct space will throw an error
    let mut mic_files: Vec<String> = Vec::with_capacity(mics as usize);
    let mut mics: Vec<[f64; 3]> = Vec::with_capacity(mics as usize);
    let mut mic_vals = m.values_of("microphone").unwrap();
    while let Some(f_n) = mic_vals.next() {
        mic_files.push(f_n.to_string());
        let x = mic_vals.next().unwrap().parse::<f64>()?;
        let y = mic_vals.next().unwrap().parse::<f64>()?;
        let z = mic_vals.next().unwrap().parse::<f64>()?;
        if !(x.is_finite() && y.is_finite() && z.is_finite()) {
            Err(RatlatError::InvalidCoordinate)?
        }
        mics.push([x, y, z]);
    }
    Ok((mic_files, mics))
}

fn bb_arg<'a, 'b>() -> Arg<'a, 'b> {
    Arg::with_name("box")
        .help("Define bounding box as two coordinates of edge points, e.g. -1 -1 1 1, given in meters. Z coordinate is assumed to be 0.")
        .long("bounding-box")
        .short("x")
        .value_names(&[ "x1", "y1", "x2","y2"])
        .multiple(false)
        .allow_hyphen_values(true)
        .required(false)
        .number_of_values(4)
}
fn get_bb(m: &ArgMatches) -> Result<Option<((f64, f64), (f64, f64))>, RatlatError> {
    //This is all wrong
    if let Some(mut z) = m.values_of("box") {
        let x1 = z.next().unwrap().parse::<f64>()?;
        let y1 = z.next().unwrap().parse::<f64>()?;
        let x2 = z.next().unwrap().parse::<f64>()?;
        let y2 = z.next().unwrap().parse::<f64>()?;
        if !(x1.is_finite() && x2.is_finite() && y1.is_finite() && y2.is_finite()) {
            Err(RatlatError::InvalidCoordinate)?
        } else if (x1 >= x2) && (y1 >= y2) {
            Err(RatlatError::InvalidCoordinate)?
        } else {
            Ok(Some(((x1, y1), (x2, y2))))
        }
    } else {
        Ok(None)
    }
}

fn ann_arg<'a, 'b>() -> Arg<'a, 'b> {
    Arg::with_name("annotation")
        .help("Use this USV annotation file, as produced by ratrec")
        .required(true)
        .takes_value(true)
        .long("annot")
        .short("a")
        .value_name("annotation file")
}

fn hm_arg<'a, 'b>() -> Arg<'a, 'b> {
    Arg::with_name("heat maps")
        .value_name("directory for heat maps ")
        .help("Triggers production of heat maps, stored in a given directory")
        .short("h")
        .long("heat-maps")
        .takes_value(true)
}
fn fix_arg<'a, 'b>() -> Arg<'a, 'b> {
    Arg::with_name("fixes")
        .value_name("file for fixes")
        .help("Triggers production of a file with coordinates of lateration fixes")
        .short("f")
        .long("fixes")
        .takes_value(true)
}
fn bin_arg<'a, 'b>() -> Arg<'a, 'b> {
    Arg::with_name("binary map")
        .value_name("file for binary map")
        .help("Triggers production of a binary file with probability maps, as a raw array of float32s in little-endian, column-first and episode-last. First occurrence DIM substring in name will be replaced by WxHxN")
        .short("b")
        .long("bin")
        .takes_value(true)
}
fn c_arg<'a, 'b>() -> Arg<'a, 'b> {
    Arg::with_name("c")
        .value_name("speed of sound")
        .help("Speed of sound in m/s")
        .default_value("344")
        .short("c")
        .takes_value(true)
}
fn get_c(m: &ArgMatches) -> Result<f64, RatlatError> {
    let ans = m.value_of("c").unwrap().parse::<f64>()?;
    if !ans.is_finite() || ans < 0. {
        Err(RatlatError::NonsenseC)?
    } else {
        Ok(ans)
    }
}
fn padding_arg<'a, 'b>() -> Arg<'a, 'b> {
    Arg::with_name("padding")
        .value_name("signal padding")
        .help("Add this many samples before and after signal duration in annotation file")
        .default_value("300")
        .short("p")
        .takes_value(true)
}
fn get_padding(m: &ArgMatches) -> Result<usize, RatlatError> {
    Ok(m.value_of("padding").unwrap().parse::<usize>()?)
}
fn smoothing_arg<'a, 'b>() -> Arg<'a, 'b> {
    Arg::with_name("smoothing")
        .value_name("smoothing")
        .help("Smooth lag over so many samples")
        .default_value("0")
        .short("s")
        .takes_value(true)
}
fn get_smoothing(m: &ArgMatches) -> Result<usize, RatlatError> {
    Ok(m.value_of("smoothing").unwrap().parse::<usize>()?)
}
fn ignorefreq_arg<'a, 'b>() -> Arg<'a, 'b> {
    Arg::with_name("ignore-freq")
        .short("i")
        .long("ignore-freq")
        .help("Ignore frequency bounds in the annotation file")
        .takes_value(false)
}
fn get_ignorefreq(m: &ArgMatches) -> bool {
    m.is_present("ignore-freq")
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn cli() {
        let m = app()
            .get_matches_from_safe(vec![
                "ratlat2",
                "-m",
                "examples/extract12_ch1.wav",
                "-.3",
                "-.3",
                ".2",
                "-m",
                "examples/extract12_ch2.wav",
                "-.3",
                ".3",
                ".2",
                "-m",
                "examples/extract12_ch3.wav",
                ".3",
                ".3",
                ".2",
                "-m",
                "examples/extract12_ch4.wav",
                ".3",
                "-.3",
                ".2",
                "-a",
                "examples/extract12_annot.txt",
                "-h",
                "/tmp/ratlat2-tests/maps",
                "-b",
                "/tmp/ratlat2-tests/bin_DIM.bin",
                "-f",
                "/tmp/ratlat2-tests/fixes.tab",
                "-s",
                "5",
            ])
            .unwrap();
        assert!(Prog::from_arg_matches(&m).is_ok());
        Prog::from_arg_matches(&m).unwrap().perform().unwrap();
    }
    #[test]
    fn nothing_to_do() {
        let m = app()
            .get_matches_from_safe(vec![
                "ratlat2",
                "-m",
                "examples/extract12_ch1.wav",
                "-.3",
                "-.3",
                ".2",
                "-m",
                "examples/extract12_ch2.wav",
                "-.3",
                ".3",
                ".2",
                "-m",
                "examples/extract12_ch3.wav",
                ".3",
                ".3",
                ".2",
                "-m",
                "examples/extract12_ch4.wav",
                ".3",
                "-.3",
                ".2",
                "-a",
                "examples/extract12_annot.txt",
            ])
            .unwrap();
        assert!(Prog::from_arg_matches(&m).unwrap().perform().is_err());
    }
}
