#[derive(Debug)]
pub enum RatlatError {
    NothingToDo,
    TruncatedAnnot,
    InvalidCoordinate,
    WavesMicsNotEqual,
    WavesDiscordant,
    WrongEpisode(String),
    CannotSaveImage,
    MultichannelUnspecified,
    MultichannelWrong(usize, usize),
    TooLittleMics,
    AnnotDiscordant,
    NonsenseC,
    InvalidEpisode,
    MarginLonger,
    IoError(std::io::Error),
    ParseFloat(std::num::ParseFloatError),
    ParseInt(std::num::ParseIntError),
    Hound(hound::Error),
    Many(Vec<RatlatError>),
}

impl std::convert::From<std::io::Error> for RatlatError {
    fn from(x: std::io::Error) -> Self {
        RatlatError::IoError(x)
    }
}
impl std::convert::From<std::num::ParseFloatError> for RatlatError {
    fn from(x: std::num::ParseFloatError) -> Self {
        RatlatError::ParseFloat(x)
    }
}
impl std::convert::From<std::num::ParseIntError> for RatlatError {
    fn from(x: std::num::ParseIntError) -> Self {
        RatlatError::ParseInt(x)
    }
}
impl std::convert::From<hound::Error> for RatlatError {
    fn from(x: hound::Error) -> Self {
        RatlatError::Hound(x)
    }
}

use std::fmt;
impl fmt::Display for RatlatError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::RatlatError::*;
        match self {
            InvalidCoordinate => write!(f, "Invalid coordinate value"),
            InvalidEpisode => write!(f, "Invalid episode number"),
            TooLittleMics => write!(f, "At least three microphones & hence files are required"),
            MultichannelUnspecified => write!(
                f,
                "Multichannel wave file given and no channel specified with ::<ch> suffix"
            ),
            MultichannelWrong(g, mx) => write!(
                f,
                "Multichannel wave file with {} channels given and channel {} specified",
                mx, g
            ),
            TruncatedAnnot => write!(f, "Truncated line in annotation file"),
            WrongEpisode(x) => write!(
                f,
                "Episode >>{}<< has wrong length or invalid frequency band",
                x
            ),
            NothingToDo => write!(f, "Nothing to do"),
            CannotSaveImage => write!(f, "Cannot save image file"),
            NonsenseC => write!(f, "Nonsense speed of sound parameter"),
            WavesMicsNotEqual => write!(f, "Different number of mics and wave files"),
            AnnotDiscordant => write!(f, "Annotation file discordant"),
            MarginLonger => write!(f, "Margin longer than file"),
            WavesDiscordant => write!(
                f,
                "Wave files are discordant, probably from different experiments"
            ),
            ParseFloat(x) => write!(f, "Parse error: {}", x),
            ParseInt(x) => write!(f, "Parse error: {}", x),
            IoError(x) => write!(f, "I/O error: {}", x),
            Hound(x) => write!(f, "Hound error: {}", x),
            Many(x) => {
                if x.is_empty() {
                    write!(f, "No errors")
                } else {
                    write!(f, "{} errors, first {}", x.len(), x[0])
                }
            }
        }
    }
}

use std::error::Error;
impl Error for RatlatError {}
