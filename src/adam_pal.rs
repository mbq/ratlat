static ADAM_PAL: [f32; 48] = [
    0., 0., 0., 0., 0., 0., 0., 0., 162., 196., 255., 255., 255., 255., 255., 255., 0., 0., 0., 0.,
    0., 128., 162., 196., 0., 0., 0., 128., 196., 255., 255., 255., 0., 64., 96., 128., 255., 0.,
    0., 0., 0., 0., 0., 0., 0., 0., 128., 255.,
];

pub fn adam_colour(value: f32) -> [u8; 3] {
    let fidx = value.max(0.).min(1.) * 15.;

    let (idx, wgt) = if fidx == 15. {
        (14, 1.)
    } else {
        (fidx.trunc() as usize, fidx - fidx.trunc())
    };
    [
        (ADAM_PAL[idx] * (1. - wgt) + ADAM_PAL[idx + 1] * wgt) as u8,
        (ADAM_PAL[idx + 16] * (1. - wgt) + ADAM_PAL[idx + 17] * wgt) as u8,
        (ADAM_PAL[idx + 32] * (1. - wgt) + ADAM_PAL[idx + 33] * wgt) as u8,
    ]
}
