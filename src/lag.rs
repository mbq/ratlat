use crate::max_filter::max_filter;
use crate::window::Window;
use claxon::FlacReader;
use fftw::array::AlignedVec;
use fftw::plan::{C2RPlan, Plan, Plan32, R2CPlan};
use fftw::types::{c32, Flag};

pub struct Ccf {
    chan: (usize, usize),
    x: Vec<f32>,
}

impl Ccf {
    pub fn smooth(&mut self, k: usize) {
        let sk = k / 2;
        self.x = max_filter(&self.x, sk, sk);
    }
    pub fn abs_iter(&self) -> impl Iterator<Item = f32> + '_ {
        self.x.iter().map(|x| x.abs())
    }
    fn convert_idx(&self, idx: usize) -> isize {
        (idx as isize) - (self.x.len() as isize) / 2
    }
    fn convert_iidx(&self, iidx: isize) -> Result<usize, ()> {
        let idx = iidx + ((self.x.len() / 2) as isize);
        if idx < 0 || idx as usize >= self.x.len() {
            Err(())
        } else {
            Ok(idx as usize)
        }
    }
    pub fn get_with_samp_lag(&self, iidx: isize) -> f32 {
        if let Ok(i) = self.convert_iidx(iidx) {
            self.x[i]
        } else {
            0.
        }
    }
    pub fn max_val(&self) -> Option<(isize, f32)> {
        self.abs_iter()
            .enumerate()
            .max_by(|(_, a), (_, b)| a.partial_cmp(b).unwrap())
            .map(|(pos, val)| (self.convert_idx(pos), val))
    }
    pub fn channels(&self) -> (usize, usize) {
        self.chan
    }
}

pub struct Lag {
    n: usize,
    smooth: usize,
    data: Vec<AlignedVec<c32>>,
    rev_plan: Plan<c32, f32, Plan32>,
    channels: u32,
    buf_a: AlignedVec<c32>,
    buf_b: AlignedVec<f32>,
}
impl Lag {
    pub fn from_vecs(
        input: &Vec<Vec<f32>>,
        smooth: usize,
        window: Window,
        freq_range: Option<(usize, usize)>,
    ) -> Lag {
        let n = input[0].len();
        let channels = input.len();

        //Used only in this function
        let mut fwd_plan: Plan<f32, c32, Plan32> = R2CPlan::aligned(&[n], Flag::ESTIMATE).unwrap();

        //Stored in object to be re-used
        let rev_plan = C2RPlan::aligned(&[n], Flag::ESTIMATE).unwrap();

        let mut buffer: AlignedVec<f32> = AlignedVec::new(n);
        let data: Vec<AlignedVec<c32>> = (0..channels)
            .map(|c| {
                //Fill aligned buffer and calculate norm
                let mut ans: AlignedVec<c32> = AlignedVec::new(n / 2 + 1);
                let ave = input[c].iter().sum::<f32>() / (input[c].len() as f32);
                for e in 0..n {
                    buffer[e] = window.val(e, n) * (input[c][e] - ave); //TODO? Window function
                }

                //Do FFT
                fwd_plan.r2c(&mut buffer, &mut ans).unwrap();

                //Filter frequencies; this is effectively FIR
                let is_freq_in_range = |fq: usize| -> bool {
                    match freq_range {
                        Some((a, b)) => (fq >= a) && (fq <= b),
                        None => true,
                    }
                };
                let mut norm = 0.; //Also calculate norm
                for e in 0..=n / 2 {
                    ans[e] = if is_freq_in_range(e) {
                        norm += ans[e].norm_sqr();
                        ans[e]
                    } else {
                        c32 { re: 0., im: 0. }
                    };
                }
                //Finish norm
                norm = (norm * 2.).sqrt();

                //Normalise complex output
                if norm > 0. {
                    for e in 0..=n / 2 {
                        ans[e] /= norm;
                    }
                }
                ans
            })
            .collect();
        Lag {
            n,
            smooth,
            rev_plan,
            channels: channels as u32,
            data,
            buf_a: AlignedVec::new(n / 2 + 1),
            buf_b: AlignedVec::new(n),
        }
    }
    pub fn channels(&self) -> usize {
        self.channels as usize
    }
    pub fn ccf(&mut self, c1: usize, c2: usize) -> Ccf {
        assert!(c1 < self.channels as usize);
        assert!(c2 < self.channels as usize);
        for e in 0..self.n / 2 + 1 {
            self.buf_a[e] = self.data[c1][e].conj() * self.data[c2][e];
        }
        self.rev_plan.c2r(&mut self.buf_a, &mut self.buf_b).unwrap();
        //So-called fftshift, so that it goes -0+ rather than 0+-
        let mut ans = Ccf {
            chan: (c1, c2),
            x: (0..self.n)
                .map(|e| self.buf_b[(e + (self.n + 1) / 2) % self.n])
                .collect(),
        };
        if self.smooth > 1 {
            ans.smooth(self.smooth);
        }
        ans
    }
    pub fn from_flac(
        f_n: &str,
        offset: usize,
        len: Option<usize>,
        window: Window,
        freq_range: Option<(f64, f64)>,
    ) -> Result<Lag, ()> {
        if let Ok(mut reader) = FlacReader::open(f_n) {
            let si = reader.streaminfo();
            let sample_rate = f64::from(si.sample_rate);
            let channels = si.channels;
            let n = len.unwrap_or_else(|| si.samples.unwrap() as usize);
            let len_secs = ((n - offset) as f64) / sample_rate;
            let fr = freq_range.map(|x| {
                (
                    (x.0 * len_secs).floor() as usize,
                    (x.1 * len_secs).ceil() as usize,
                )
            });

            let mut input: Vec<Vec<f32>> = (0..channels).map(|_| Vec::with_capacity(n)).collect();

            let it = reader
                .samples()
                .skip(offset * (channels as usize))
                .take(n * (channels as usize));
            for (i, v) in it.enumerate() {
                let ch = i % (channels as usize);
                input[ch].push(v.unwrap() as f32);
            }
            Ok(Lag::from_vecs(&input, 0, window, fr))
        } else {
            Err(())
        }
    }
}

#[cfg(test)]
mod tests {
    use super::Lag;
    use super::Window;
    fn make_spike(n: usize, center: usize) -> Vec<f32> {
        (0..n)
            .map(|i| {
                let x = i as f32 - center as f32;
                1. / (1. + x * x)
            })
            .collect()
    }
    #[test]
    fn spikes() {
        let xx: Vec<Vec<f32>> = [511, 451].iter().map(|x| make_spike(1000, *x)).collect();
        let mut lag = Lag::from_vecs(&xx, 0, Window::Rectangular, None);
        let a = lag.ccf(0, 1).max_val().unwrap();
        assert!(a.1 > 0.9);
        assert_eq!(a.0, -60);
        let a = lag.ccf(0, 0).max_val().unwrap();
        assert!(a.1 > 0.9);
        assert_eq!(a.0, 0);
        let a = lag.ccf(1, 0).max_val().unwrap();
        assert!(a.1 > 0.9);
        assert_eq!(a.0, 60);
    }
    #[test]
    fn flacs() {
        let f_n: String = "examples/samp_4_chan_one.flac".to_string();
        let mut lag = Lag::from_flac(&f_n, 0, None, Window::Rectangular, None).unwrap();
        //NB. Correlations span from .22-.31
        assert_eq!(lag.ccf(0, 1).max_val().unwrap().0, 71);
        assert_eq!(lag.ccf(0, 2).max_val().unwrap().0, -153);
        assert_eq!(lag.ccf(0, 3).max_val().unwrap().0, -53);
        assert_eq!(lag.ccf(1, 2).max_val().unwrap().0, -76);
        assert_eq!(lag.ccf(1, 3).max_val().unwrap().0, -314);
        assert_eq!(lag.ccf(2, 3).max_val().unwrap().0, 17);
    }
    #[test]
    fn squeak_nofreq() {
        let f_n: String = "examples/squeak_4chan_mostly.flac".to_string();
        let mut lag = Lag::from_flac(&f_n, 4854, Some(16195), Window::Rectangular, None).unwrap();
        for e in 0..4 {
            for ee in (e + 1)..4 {
                println!("ch{}->ch{} {:?}", e + 1, ee + 1, lag.ccf(e, ee).max_val());
            }
        }
    }
    //30-70kHz
    #[test]
    fn squeak_freq() {
        let f_n: String = "examples/squeak_4chan_mostly.flac".to_string();
        let mut lag = Lag::from_flac(
            &f_n,
            4854,
            Some(16195),
            Window::Rectangular,
            Some((30_000., 70_000.)),
        )
        .unwrap();
        for e in 0..4 {
            for ee in (e + 1)..4 {
                println!("ch{}->ch{} {:?}", e + 1, ee + 1, lag.ccf(e, ee).max_val());
            }
        }
    }
}
