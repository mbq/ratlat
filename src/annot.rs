use crate::err::RatlatError;

#[derive(Debug)]
pub struct Episode {
    pub ts: usize,
    pub te: usize,
    pub fr: Option<(usize, usize)>,
}
impl Episode {
    fn from_string(
        buf: &String,
        sampling_rate: f64,
        ignore_freq: bool,
    ) -> Result<Episode, RatlatError> {
        let mut iter = buf
            .split(char::is_whitespace)
            .filter(|x| !x.is_empty())
            .peekable();
        let ts = iter
            .next()
            .ok_or(RatlatError::TruncatedAnnot)?
            .parse::<usize>()?;
        let te = iter
            .next()
            .ok_or(RatlatError::TruncatedAnnot)?
            .parse::<usize>()?;
        let fr = if let Some(_) = iter.peek() {
            if !ignore_freq {
                let fl = iter
                    .next()
                    .ok_or(RatlatError::TruncatedAnnot)?
                    .parse::<f64>()?;
                let fh = iter
                    .next()
                    .ok_or(RatlatError::TruncatedAnnot)?
                    .parse::<f64>()?;
                if fl < 0. || fl >= fh || fh > sampling_rate / 2. {
                    Err(RatlatError::WrongEpisode(buf.into()))?;
                }
                let freq_to_fft_idx = |fq| fq * ((te - ts) as f64) / sampling_rate;

                Some((
                    freq_to_fft_idx(fl).floor() as usize,
                    freq_to_fft_idx(fh).ceil() as usize,
                ))
            } else {
                None
            }
        } else {
            None
        };

        if ts >= te {
            Err(RatlatError::WrongEpisode(buf.into()))?;
        }
        Ok(Episode { ts, te, fr })
    }
}

#[derive(Debug)]
pub struct AnnotFile {
    pub episodes: Vec<Episode>,
}

use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
impl AnnotFile {
    pub fn from_file(
        f_n: &str,
        sampling_rate: f64,
        ignore_freq: bool,
    ) -> Result<AnnotFile, RatlatError> {
        let mut stream = BufReader::new(File::open(f_n)?);
        let mut buf = String::new();
        let mut episodes = Vec::new();
        loop {
            if stream.read_line(&mut buf)? == 0 {
                break;
            }
            episodes.push(Episode::from_string(&buf, sampling_rate, ignore_freq)?);
            buf.clear();
        }
        Ok(AnnotFile { episodes })
    }
    pub fn episodes(&self) -> usize {
        self.episodes.len()
    }
    pub fn max_sample(&self) -> usize {
        self.episodes.iter().map(|x| x.te).max().unwrap_or(0)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn parse_example() {
        let sr = 250000.;
        let x = AnnotFile::from_file("examples/sample_annot.txt", sr, false).unwrap();
        assert_eq!(x.episodes(), 11);
        assert_eq!(x.episodes[4].ts, (62895.104 / 1000. * sr).round() as usize);
        assert_eq!(x.episodes[4].te, (62956.544 / 1000. * sr).round() as usize);
        assert_eq!(x.episodes[4].fr, Some((2325, 2955)));
        assert_eq!(x.episodes[5].fr, None);
    }
}
