#[derive(Clone, Copy)]
pub enum Window {
    Hann,
    Rectangular,
}
impl Window {
    pub fn val(&self, i: usize, n: usize) -> f32 {
        let xn = (i as f32) / (n as f32 - 1.) * std::f32::consts::PI * 2.;
        match self {
            Window::Rectangular => 1.,
            Window::Hann => 0.5 - 0.5 * xn.cos(),
        }
    }
}
