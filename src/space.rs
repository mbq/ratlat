use crate::err::RatlatError;
use crate::lag::{Ccf, Lag};
use std::io::Write;

#[derive(Debug)]
pub struct Space {
    w: usize,
    h: usize,
    n: usize,
    microphones: Vec<[f64; 3]>,
    speed_of_sound: f64,
    sampling: f64,
    b_box: ((f64, f64), (f64, f64)),
}
pub struct ProbMap<'a> {
    space: &'a Space,
    ccfs: Vec<Ccf>,
}
impl<'a> ProbMap<'a> {
    pub fn get_val(&self, p: (f64, f64)) -> f32 {
        self.ccfs
            .iter()
            .map(|x| {
                let iidx = self.space.point_to_lag(p, x.channels());
                x.get_with_samp_lag(iidx)
            })
            .sum::<f32>()
    }
    pub fn render_bin<W: Write>(&self, target: &mut W) -> Result<(), RatlatError> {
        let (w, h) = self.space.dim();
        let ((ix, iy), (ax, ay)) = self.space.bounding_box();
        for px in 0..w {
            for py in 0..h {
                let sx = (px as f64) / (w as f64);
                let sy = (py as f64) / (h as f64);
                let x = ix + (ax - ix) * sx;
                let y = iy + (ay - iy) * sy;
                let val = self.get_val((x, y)).abs();
                let enc = val.to_bits().to_le_bytes();
                target.write(&enc)?;
            }
        }
        Ok(())
    }
    pub fn render_image(&self, f_n: &str) -> Result<(), RatlatError> {
        use crate::adam_pal::adam_colour;
        use image::ImageBuffer;
        let ((ix, iy), (ax, ay)) = self.space.bounding_box();
        let w = self.space.w as u32;
        let h = self.space.h as u32;
        let nrm = self
            .ccfs
            .iter()
            .map(|x| x.max_val().unwrap().1)
            .sum::<f32>();

        let mut spike: Option<(u32, u32, f32)> = None;
        let mut img = ImageBuffer::new(w, h);
        for (px, py, pix) in img.enumerate_pixels_mut() {
            let sx = (px as f64) / (w as f64);
            let sy = ((h - 1 - py) as f64) / (h as f64);
            let x = ix + (ax - ix) * sx;
            let y = iy + (ay - iy) * sy;
            let val = self.get_val((x, y)).abs() / nrm;
            spike = if let Some((_, _, mv)) = spike {
                if val > mv {
                    Some((px, py, val))
                } else {
                    spike
                }
            } else {
                Some((px, py, val))
            };
            *pix = image::Rgb(adam_colour(sq(val)));
        }
        if let Some((pmx, pmy, _)) = spike {
            for px in 0..w {
                *img.get_pixel_mut(px, pmy) = image::Rgb([255, 255, 255]);
            }
            for py in 0..h {
                *img.get_pixel_mut(pmx, py) = image::Rgb([255, 255, 255]);
            }
        }
        match img.save(f_n) {
            Ok(_) => Ok(()),
            Err(_) => Err(RatlatError::CannotSaveImage),
        }
    }
    pub fn get_max(&self) -> Option<(f64, f64, f32)> {
        let ((ix, iy), (ax, ay)) = self.space.bounding_box();
        let ds = self.space.speed_of_sound / self.space.sampling;
        let mut x = ix;
        let mut y = iy;
        //TODO: Make an actual iterator...
        let mut pval: f32 = -1.;
        let mut peak: Option<(f64, f64, f32)> = None;
        let nrm = self.ccfs.len() as f32;
        loop {
            let val = self.get_val((x, y)).abs() / nrm;
            if val > pval {
                peak = Some((x, y, val));
                pval = val;
            }
            x += ds;
            if x > ax {
                if y < ay {
                    x = ix;
                    y += ds;
                } else {
                    break;
                }
            }
        }
        peak
    }
}

use std::ops::Mul;
fn sq<T: Copy + Mul<Output = T>>(x: T) -> T {
    x * x
}

impl Space {
    //TODO: Scaling factor~1., to scale the bitmap up or down
    pub fn dim(&self) -> (usize, usize) {
        (self.w, self.h)
    }
    pub fn bounding_box(&self) -> ((f64, f64), (f64, f64)) {
        self.b_box
    }
    pub fn new<I>(
        mics: I,
        c: f64,
        sampling: f64,
        b_box: ((f64, f64), (f64, f64)),
    ) -> Result<Space, RatlatError>
    where
        I: IntoIterator<Item = [f64; 3]>,
    {
        assert!(c > 0.);
        assert!(sampling > 0.);
        let microphones: Vec<[f64; 3]> = mics.into_iter().collect();
        if microphones.is_empty() {
            Err(RatlatError::TooLittleMics)?;
        }
        let ((ix, iy), (ax, ay)) = b_box;
        let (dx, dy) = (ax - ix, ay - iy);
        let (w, h) = (
            (dx / c * sampling).round() as usize,
            (dy / c * sampling).round() as usize,
        );
        let n = w * h;

        Ok(Space {
            w,
            h,
            n,
            microphones,
            speed_of_sound: c,
            sampling,
            b_box,
        })
    }
    fn point_to_lag(&self, p: (f64, f64), c: (usize, usize)) -> isize {
        let mic0 = self.microphones[c.0];
        let mic1 = self.microphones[c.1];
        let d0 = (sq(mic0[0] - p.0) + sq(mic0[1] - p.1) + sq(mic0[2])).sqrt();
        let d1 = (sq(mic1[0] - p.0) + sq(mic1[1] - p.1) + sq(mic1[2])).sqrt();
        //This is OK, consistent with how we measure lag
        ((d1 - d0) / self.speed_of_sound * self.sampling).round() as isize
    }
    pub fn sampling(&self) -> f64 {
        self.sampling
    }
    pub fn laterate(&self, lag: &mut Lag) -> ProbMap<'_> {
        assert_eq!(self.microphones.len(), lag.channels());
        let mut ccfs: Vec<Ccf> = Vec::new();
        //TODO: Limit ccf-s to only given channel pairs
        for f in 0..lag.channels() {
            for t in (f + 1)..lag.channels() {
                ccfs.push(lag.ccf(f, t));
            }
        }
        ProbMap { space: &self, ccfs }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::window::Window;
    #[test]
    fn on_squeak() {
        let mics: [[f64; 3]; 4] = [
            [-0.3, -0.3, 0.2],
            [-0.3, 0.3, 0.2],
            [0.3, 0.3, 0.2],
            [0.3, -0.3, 0.2],
        ];
        let f_n: String = "examples/squeak_4chan_mostly.flac".to_string();
        let mut lag =
            Lag::from_flac(&f_n, 0, None, Window::Hann, Some((30_000., 70_000.))).unwrap();
        let space = Space::new(
            mics.iter().cloned(),
            344.,
            250000.,
            ((-0.3, -0.3), (0.3, 0.3)),
        )
        .unwrap();
        let map = space.laterate(&mut lag);
        let ans = map.get_max();
        map.render_image("/tmp/lat_sq.png").unwrap();
        println!("{:?}", ans);
    }

    fn make_spike(n: usize, center: usize) -> Vec<f32> {
        (0..n)
            .map(|i| {
                let x = i as f32 - center as f32;
                1. / (1. + x * x)
            })
            .collect()
    }
    #[test]
    fn spikes() {
        //Square mics
        //2 3
        //1 4
        let mics: [[f64; 3]; 4] = [[-1., -1., 0.], [-1., 1., 0.], [1., 1., 0.], [1., -1., 0.]];
        let x = -0.5;
        let y = 0.75;
        let off = 1000;
        let c = 344.;
        let sr = 25000.;

        let xx: Vec<Vec<f32>> = mics
            .iter()
            .map(|m| {
                let mx = m[0];
                let my = m[1];
                let d = ((x - mx) * (x - mx) + (y - my) * (y - my)).sqrt();
                let tof = d / c;
                let tof_samples = (tof * sr).round() as usize;
                make_spike(2000, off + tof_samples)
            })
            .collect();
        let mut lag = Lag::from_vecs(&xx, 0, Window::Rectangular, None);
        let space = Space::new(mics.iter().cloned(), c, sr, ((-1., -1.), (1., 1.))).unwrap();
        let map = space.laterate(&mut lag);
        let ans = map.get_max();
        assert!((ans.unwrap().0 - x).abs() < 0.1);
        assert!((ans.unwrap().1 - y).abs() < 0.1);
        assert!(ans.unwrap().2 > 0.9);
        assert!(ans.unwrap().2 <= 1.1); //.1 is for numeric errors
        map.render_image("/tmp/lat_spike.png").unwrap();
        println!("{:?}", ans);
    }
}
